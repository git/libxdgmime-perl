// exported functions - parseable by h2xs

const char  *xdg_mime_get_mime_type_for_data       (const void *data,
						    unsigned long len);
const char  *xdg_mime_get_mime_type_for_file       (const char *file_name,
                                                    struct stat *statbuf);
const char  *xdg_mime_get_mime_type_from_file_name (const char *file_name);
int          xdg_mime_is_valid_mime_type           (const char *mime_type);
int          xdg_mime_mime_type_equal              (const char *mime_a,
						    const char *mime_b);
int          xdg_mime_media_type_equal             (const char *mime_a,
						    const char *mime_b);
int          xdg_mime_mime_type_subclass           (const char *mime_a,
						    const char *mime_b);
int          xdg_mime_get_max_buffer_extents       (void);
void         xdg_mime_shutdown                     (void);
void         xdg_mime_dump                         (void);

