Updating the bindings
#####################

* The upstream sources are added https://gitlab.freedesktop.org/xdg/xdgmime as
  git-subtree in xdgmime-source use git-subtree to update them

* All functions in upstream's xdgmime.h are also present as CPP defines (for
  adding XDG_PREFIX to the function name, which confuses h2xs into creating
  constant symbols for them. These constant symbols then create errors in the
  automatically generated tests.

* By running `make update-bindings` h2xs is called with the appropriate
  parameters to create a fresh xs module from xdgmime.h. Only symbols matching
  XDG_ are treated as constants (-M '^XDG'), thus avoiding the problematic
  defines.
  Rationale of relevant h2xs parameters:
  * -P - avoids POD part in .pm - which only contains boilerplate and would need
    adopting every time
  * -F -DHAVE_MMAP - simply sets the define during compilation
  * -M '^XDG_' - treat only defines starting with XDG as constants (see above)
  * -t PV - set the default constant type to PV (a.k.a. string see man perlguts)
    (fitting with the 3 XDG_ constants)

* All functions needed in the perl-module need to be added to
  Xdgmime/lib/Xdgmime.pl in the EXPORT_TAGS (and since pmg-api and others use
  them without fully qualifying them with the module name also to the EXPORT)
  array, and implemented in Xdgmime/Xdgemime.xs

* C::Scan is not used anymore but can be installed via CPAN, should you need
  an initial prototype for a new function

0.) initial package generated with following commands:
######################################################

git clone git://anongit.freedesktop.org/xdg/xdgmime


man h2xs
man perlxs
man perlxstut

h2xs -Afn Xdgmime

cp xdgmime-source/src/xdgmime*.[ch] Xdgmime/

create exports.h file in Xdgmime/exports.h

# Note: following requires package 'libc-scan-perl' 
# downloaded that from debian etch archive

h2xs -Oxan Xdgmime exports.h

edit Xdgmime/Xdgmime.xs
edit Xdgmime/lib/Xdgmime.pm
edit Xdgmime/Makefile (add -DHAVE_MMAP)

