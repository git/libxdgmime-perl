#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include <xdgmime.h>

#include "const-c.inc"

MODULE = Xdgmime		PACKAGE = Xdgmime		

INCLUDE: const-xs.inc

void
xdg_mime_dump()

int
xdg_mime_get_max_buffer_extents()

const char *
xdg_mime_get_mime_type_for_data(data, len)
	const char * data
	unsigned long	len
	CODE:
		int result_prio;
	   	RETVAL = xdg_mime_get_mime_type_for_data (data, len, &result_prio);
	OUTPUT:
            RETVAL

const char *
xdg_mime_get_mime_type_for_file(file_name)
	const char *	file_name
	CODE:
	   	RETVAL = xdg_mime_get_mime_type_for_file (file_name, NULL);
	OUTPUT:
            RETVAL

const char *
xdg_mime_get_mime_type_from_file_name(file_name)
	const char *	file_name
	OUTPUT:
            RETVAL

int
xdg_mime_is_valid_mime_type(mime_type)
	const char *	mime_type

int
xdg_mime_media_type_equal(mime_a, mime_b)
	const char *	mime_a
	const char *	mime_b

int
xdg_mime_mime_type_equal(mime_a, mime_b)
	const char *	mime_a
	const char *	mime_b

int
xdg_mime_mime_type_subclass(mime_a, mime_b)
	const char *	mime_a
	const char *	mime_b

void
xdg_mime_shutdown()

SV *    
xdg_mime_list_mime_parents (mime_type)
	const char *	mime_type
    INIT:
	AV * results;
  	char **parents;
  	char **p;
        results = (AV *)sv_2mortal((SV *)newAV());
    CODE:
	parents = xdg_mime_list_mime_parents (mime_type);	
  	for (p = parents; p && *p; p++) {
	      av_push(results, newSVpv(*p, 0));
    	}
	free (parents);
	RETVAL = newRV((SV *)results);
    OUTPUT:
        RETVAL
