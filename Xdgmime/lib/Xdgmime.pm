package Xdgmime;

use 5.036000;
use strict;
use warnings;
use Carp;

require Exporter;
use AutoLoader;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Xdgmime ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	XDG_MIME_TYPE_EMPTY
	XDG_MIME_TYPE_TEXTPLAIN
	XDG_MIME_TYPE_UNKNOWN
	xdg_mime_dump
	xdg_mime_get_max_buffer_extents
	xdg_mime_get_mime_type_for_data
	xdg_mime_get_mime_type_for_file
	xdg_mime_get_mime_type_from_file_name
	xdg_mime_is_valid_mime_type
	xdg_mime_list_mime_parents
	xdg_mime_media_type_equal
	xdg_mime_mime_type_equal
	xdg_mime_mime_type_subclass
	xdg_mime_shutdown
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	XDG_MIME_TYPE_EMPTY
	XDG_MIME_TYPE_TEXTPLAIN
	XDG_MIME_TYPE_UNKNOWN
	xdg_mime_dump
	xdg_mime_get_max_buffer_extents
	xdg_mime_get_mime_type_for_data
	xdg_mime_get_mime_type_for_file
	xdg_mime_get_mime_type_from_file_name
	xdg_mime_is_valid_mime_type
	xdg_mime_list_mime_parents
	xdg_mime_media_type_equal
	xdg_mime_mime_type_equal
	xdg_mime_mime_type_subclass
	xdg_mime_shutdown
);

our $VERSION = '1.1';

sub AUTOLOAD {
    # This AUTOLOAD is used to 'autoload' constants from the constant()
    # XS function.

    my $constname;
    our $AUTOLOAD;
    ($constname = $AUTOLOAD) =~ s/.*:://;
    croak "&Xdgmime::constant not defined" if $constname eq 'constant';
    my ($error, $val) = constant($constname);
    if ($error) { croak $error; }
    {
	no strict 'refs';
	# Fixed between 5.005_53 and 5.005_61
#XXX	if ($] >= 5.00561) {
#XXX	    *$AUTOLOAD = sub () { $val };
#XXX	}
#XXX	else {
	    *$AUTOLOAD = sub { $val };
#XXX	}
    }
    goto &$AUTOLOAD;
}

require XSLoader;
XSLoader::load('Xdgmime', $VERSION);

# Preloaded methods go here.

# Autoload methods go after __END__, and are processed by the autosplit program.

1;
__END__
