include /usr/share/dpkg/default.mk

PACKAGE=libxdgmime-perl

BUILDSRC=$(PACKAGE)-$(DEB_VERSION)
DSC = $(PACKAGE)_$(DEB_VERSION).dsc

DEB = $(PACKAGE)_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb
DEB_DBG = $(PACKAGE)-dbgsym_$(DEB_VERSION)_$(DEB_HOST_ARCH).deb

OPKGNAME=Xdgmime
UPSTREAM=xdgmime-source/src

all: $(DEB)
.PHONY: update-bindings
update-bindings:
	cp -a $(UPSTREAM)/xdgmime*.[ch] $(OPKGNAME)/
	h2xs -P -v $(DEB_VERSION_UPSTREAM) -F -DHAVE_MMAP -M '^XDG_' -t PV -Ofan Xdgmime xdgmime.h
	rm -f $(OPKGNAME)/xdgmime*.[ch]
	echo "Please manually check and add the diff of the automatically generated update"

.PHONY: $(BUILDSRC)
$(BUILDSRC):
	-rm -rf $@ $@.tmp
	cp -a $(OPKGNAME) $@.tmp
	cp -a $(UPSTREAM)/xdgmime*.[ch] $@.tmp/
	perl -MDevel::PPPort -e 'Devel::PPPort::WriteFile("$@.tmp/ppport.h");'
	cp -a debian $@.tmp
	mv $@.tmp $@

.PHONY: dsc
dsc:
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(BUILDSRC)
	cd $(BUILDSRC); dpkg-buildpackage -S -us -uc -d

.PHONY: sbuild
sbuild: $(DSC)
	sbuild $(DSC)

.PHONY: deb
deb: $(DEB)
$(DEB_DBG): $(DEB)
$(DEB): $(OPKGNAME)/Xdgmime.xs $(BUILDSRC)
	cd $(BUILDSRC); dpkg-buildpackage -b -us -uc
	lintian $(DEB) $(DEB_DBG)

.phony: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEB) $(DEB_DBG)
	tar cf - $(DEB) $(DEB_DBG) | ssh -X repoman@repo.proxmox.com -- upload --product pmg --dist $(UPLOAD_DIST)

.PHONY: clean
clean:
	rm -rf build $(PACKAGE)-[0-9]*/
	rm -f *.deb *.buildinfo *.build *.dsc *.changes $(PACKAGE)*tar*

.PHONY: dinstall
dinstall: $(DEB)
	dpkg -i $(DEB)
